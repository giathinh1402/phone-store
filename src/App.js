import React,{Component} from 'react';
import Headers from './containers/components/Header/Header';

import './App.css';




class App extends Component {
    
    render(){
        return (
            <div>     
                <main id="mainContainer">
                    <div className="container"> 
                        <Headers />
                    </div>
                </main>
                
            </div>
        );
    }
}

export default App;
