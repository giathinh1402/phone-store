import React,{Component} from 'react';
import {BrowserRouter, Route, Switch , Link} from 'react-router-dom' ;
import Home from './../../page/homePage/Home';
import ListProduct from './../../page/homePage/ListProduct';
import CartContainer from '../cart/CartContainer';
import Login from '../../page/homePage/Login';
import Register from '../../page/homePage/Register';
import NotFound from '../../page/notfound/NotFound';

class Header extends Component {
    render(){
        return ( 
            <BrowserRouter>
                <div className="d-flex justify-content-center  " id="wrapper">
            {/* Logo */}
                    <div className="bg-light border-right" id="sidebar-wrapper">
                        <div className="list-group list-group-flush">
                        
                        <img src="http://tiepthitieudung.com/upload_images/images/18/11/29/1b1o122.jpg" 
                        alt="" width="250" height="130" />
                        <h1><center>Phone Store</center></h1>      
                        </div>
                    </div>
            {/* End Logo */}
    
                    <div id="page-content-wrapper">
                        {/* Navbar */}
                        <nav className="navbar navbar-expand-lg navbar-light bg-light border-bottom">
                            
                            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span className="navbar-toggler-icon"></span>
                            </button>

                            <div className="collapse navbar-collapse " id="navbarSupportedContent">
                                <ul className="navbar-nav ml-auto mt-2 mt-lg-3">
                                    
                                    <li className="nav-item ">
                                        <Link className="nav-link" to="/"><i className="fas fa-home"></i> Home</Link>
                                    </li>
                                    <li className="nav-item">
                                        <Link className="nav-link" to="/product-list"><i className="fas fa-list-ul"></i> List Product </Link>
                                    </li>
                                    <li className="nav-item">
                                        <Link className="nav-link" to="/cart"><i className="fas fa-shopping-cart"></i> Cart </Link>
                                    </li>
                                    <li className="nav-item">
                                        <Link className="nav-link" to="/login"><i className="fas fa-user-circle"></i> Login </Link>
                                    </li>
                                    <li className="nav-item">
                                        <Link className="nav-link" to="/register"><i className="fas fa-user-circle"></i> Register </Link>
                                    </li>
                                    <li className="nav-item">
                                        <Link className="nav-link btn btn-floating purple lighten-1 " to="/"><h4>NT</h4></Link>
                                    </li>
                                    <li>
                                        <form className="navbar-form " role="search">
                                            <div className="input-group">
                                                <input type="text" className="form-control" placeholder="Search" />
                                                <button type="submit" className="btn btn-default"><i className="fas fa-search"></i></button>
                                            </div>  
                                        </form>
                                    </li> 
                                </ul>
                            </div>
                        </nav>
                    </div>
            {/* End Navbar */} 
                </div>
            <Switch>
                < Route exact path="/" component={Home} ></Route>
                < Route path="/product-list" component={ListProduct} ></Route>
                < Route path="/cart" component={CartContainer} ></Route>
                < Route path="/login" component={Login} ></Route>
                < Route path="/register" component={Register} ></Route>
                
                < Route component={NotFound} />
            </Switch>
                
               
            </BrowserRouter>

                

        );
    }
}

export default Header;
