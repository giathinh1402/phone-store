import React, { Component } from 'react'
import CartContainer from '../../components/cart/CartContainer';
import MessageContainer from '../../components/message/MessageContainer';

class Cart extends Component {
    render() {
        return (
            <div>
                <MessageContainer />
                <CartContainer />
                
               
            </div>
        )
    }
}

export default Cart;
