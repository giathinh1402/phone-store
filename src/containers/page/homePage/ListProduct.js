import React, { Component } from 'react'
import ProductsContainer from'./../../components/products/ProductsContainer';
import MessageContainer from '../../components/message/MessageContainer';

class ListProduct extends Component {
    render() {
        return (
            <div className="container">
                <MessageContainer />
                <ProductsContainer/>
                
                
            </div>
        )
    }
}

export default ListProduct;
