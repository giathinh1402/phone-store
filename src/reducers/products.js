var initialState = [
    {
        id: 1,
        name: 'iphone7 plus',
        image: 'https://cdn1.vienthonga.vn/image/2017/3/5/iphone-7-plus-red-1.jpg',
        description: 'San pham do Apple san xuat',
        price: 500,
        inventor: 10,
        rating: 4
    },
    {
        id: 2,
        name: 'Xiaomi Redmi Note 7',
        image: 'http://www.directd.com.my/images/thumbs/0027464_xiaomi-redmi-note-7-original-xiaomi-malaysia-set_600.jpeg',
        description: 'San pham do Xiaomi san xuat',
        price: 600,
        inventor: 15,
        rating: 5
    },
    {
        id: 3,
        name: 'Realme X',
        image: 'https://www.gizmochina.com/wp-content/uploads/2019/05/Realme-X-Master-Edition.jpg',
        description: 'San pham do Oppo san xuat',
        price: 450,
        inventor: 20,
        rating: 4
    },
    {
        id: 4,
        name: 'Iphone XS Max',
        image: 'https://cdn.tgdd.vn/Products/Images/42/190322/iphone-xs-max-256gb-white-600x600.jpg',
        description: 'San pham do Apple san xuat',
        price: 450,
        inventor: 20,
        rating: 5
    },
    {
        id: 4,
        name: 'Huawei P20 Pro',
        image: 'https://www.dateks.lv/images/pic/600/600/023/141.jpg',
        description: 'San pham do Huawei san xuat',
        price: 600,
        inventor: 20,
        rating: 4
    },
    {
        id: 6,
        name: 'SamSung S10+',
        image: 'https://cdn.tgdd.vn/Products/Images/42/203207/samsung-galaxy-s10-plus-128gb-bac-da-sac-600x600.jpg',
        description: 'San pham do SamSung san xuat',
        price: 700,
        inventor: 20,
        rating: 4
    },

];

const products = (state = initialState, action) =>{
    switch(action.type){
        default: return [...state];
    }
}

export default products;